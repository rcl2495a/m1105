# Chronophone

Chronophone est un site web traitant de l'histoire du téléphone, et réalisé dans le cadre du projet tuteuré de l'enseignement M1105 pour l'IUT Informatique Paul Sabatier de Toulouse.

Le site est disponible [à cette adresse](https://valkmusic.fr/dev/ptut).

Crédits:
- Chaudon Guillaume
- Delapierre Gauvain
- Milor Liam
- Rech Loïc